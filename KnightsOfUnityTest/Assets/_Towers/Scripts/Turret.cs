﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : MonoBehaviour
{
    [Header("Variables")]
    public int numberOfShots = 12;
    [Header("Objects")]
    public GameObject bulletPrefab;
    public Transform bulletPosition;
    public GameObject bodyColor;
    public Color activeColor;
    public Color inactiveColor;
    [Header("First turret spawn")]
    public bool isSpawned = true; //in Unity Editor first turret have this at false
    private bool isOnBulletTime = false;
    
    public void Start()
    {
        //Setting up - starting as inactive color
        ChangeColor(inactiveColor);
        StartCoroutine(RotateTurretCoroutine());
    }

    public void Update()
    {
        if (TurretManager.Singleton.allowBulletTime && !isOnBulletTime)
        {
            StopAllCoroutines();
            numberOfShots = 12;
            isSpawned = false;
            isOnBulletTime = true;
            StartCoroutine(RotateTurretCoroutine()); 
        }
        if (numberOfShots <= 0) 
        {
            ChangeColor(inactiveColor);
            StopCoroutine(RotateTurretCoroutine());
        }
    }

    IEnumerator RotateTurretCoroutine()
    {
        if(isSpawned) //if new turret is spawned wait 6 sec
        {
            ChangeColor(inactiveColor);
            yield return new WaitForSeconds(6f);
        }

        while(numberOfShots > 0) //Loop for 12 shots
        {
            ChangeColor(activeColor);

            yield return new WaitForSeconds(0.5f);

            this.transform.Rotate(Vector3.forward, Random.Range(15, 45));
            Shoot();
        }     
    }


    private void Shoot()
    {
        if (bulletPrefab) //Creating bullet in facing direction
        {
            GameObject bulletCopy = Instantiate(bulletPrefab, bulletPosition.position, transform.rotation) as GameObject;
            bulletCopy.GetComponent<BulletSetup>().originalPos = bulletPosition.position;
            numberOfShots--;
        }        
    }

    private void ChangeColor(Color color)
    {
        bodyColor.GetComponent<SpriteRenderer>().color = color;
    }
}
