﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletSetup : MonoBehaviour
{
    public GameObject turretPrefab;
    [HideInInspector]
    public Vector3 originalPos;
    private Vector3 prevPos;

    private float distance;
    [SerializeField]
    private float speed = 40f;
    private bool hasCollide = false;

    private void Start()
    {
        //Setting random distance
        distance = Random.Range(1f, 4f);
    }
    
    private void Update()
    {
        prevPos = transform.position;
        transform.Translate(0f, speed * Time.deltaTime, 0f);

        RaycastHit[] hits = Physics.RaycastAll(new Ray(prevPos, (transform.position - prevPos).normalized), (transform.position - prevPos).magnitude);
        for(int i=0;i<hits.Length;i++)
        {
            if(hits[i].collider.CompareTag("Player") && hasCollide == false)
            {
                hasCollide = true;
                TurretManager.Singleton.DecreaseTowerCount();
                Destroy(hits[i].collider.gameObject);
                Destroy(gameObject);
            }
        }

        //Checking if random distance was reached
        if (Vector3.Distance(transform.position, originalPos) > distance)
        {
            //Checking if that bullet can spawn turret
            if (TurretManager.Singleton.allowTurretSpawn)
            {
                TurretManager.Singleton.IncreaseTowerCount();
                Instantiate(turretPrefab, transform.position, Quaternion.identity);
            }

            Destroy(gameObject);
        }
    }
}
