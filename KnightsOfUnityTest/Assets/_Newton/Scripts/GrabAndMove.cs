﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabAndMove : MonoBehaviour
{
    public float zCoordOffset = 10f;

    private bool isDragging = false;
    private GameObject pickedObject;

    private void Update()
    {
        if(Input.GetMouseButton(0))
        {
            //Getting ball gameobject
            if (!isDragging)
            {
                pickedObject = GetObjectFromMouseRayCast();
                if(pickedObject)
                {
                    pickedObject.GetComponent<Rigidbody>().isKinematic = true;
                    isDragging = true;
                }
            }
            //Moving already picked ball
            else if( pickedObject != null) 
            {
                pickedObject.GetComponent<Rigidbody>().MovePosition(CalcMouse3DVector());
            }
        }
        else
        {
            //Dropping Ball
            if(pickedObject != null)
            {
                pickedObject.GetComponent<Rigidbody>().isKinematic = false;
            }
            isDragging = false;
            pickedObject = null;
        }
    }

    private Vector3 CalcMouse3DVector()
    {
        Vector3 v3 = Input.mousePosition;
        v3.z = zCoordOffset;
        v3 = Camera.main.ScreenToWorldPoint(v3);
        return v3;
    }

    private GameObject GetObjectFromMouseRayCast()
    {
        GameObject obj = null;
        RaycastHit hitInfo = new RaycastHit();
        bool hit = Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo);
        if(hit)
        {
            if(hitInfo.collider.CompareTag("Player"))
            {
                obj = hitInfo.collider.gameObject;
            }
        }
        return obj;
    }
}
