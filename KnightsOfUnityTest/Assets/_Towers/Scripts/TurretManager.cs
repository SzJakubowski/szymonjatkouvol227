﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TurretManager : MonoBehaviour
{
    //Singleton for simplicity
    private static TurretManager _instance;
    public static TurretManager Singleton { get { return _instance; } }

    [Header("references")]
    public TMP_Text towerCountText;
    [HideInInspector]
    public bool allowTurretSpawn = true;
    [HideInInspector]
    public bool allowBulletTime = false;
    private int towerMaxCount = 100;
    private int towerCount = 1;

    private void Awake()
    {
        //Singleton part 2
        if(_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    private void Update()
    {
        towerCountText.text = towerCount.ToString();
        //Checking if there's 100 towers
        if (towerCount >= towerMaxCount)
        {
            allowTurretSpawn = false;
            allowBulletTime = true;
        }
    }
    public void IncreaseTowerCount()
    {
        towerCount++; 
    }
    public void DecreaseTowerCount()
    {
        towerCount--;
    }
}
